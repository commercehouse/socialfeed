<?php

use Coho\SocialFeed;
use SparkPost\SparkPost;
use GuzzleHttp\Client;
use Ivory\HttpAdapter\Guzzle6HttpAdapter;

$feed = new SocialFeed([
    'instagram' => [
        'client_id' => '[client_id]',
        'client_secret' => '[client_secret]',
        'limit' => 10,
        'token_expired_email' => '[token_expired_email]'
    ],
    'youtube' => [
        'developer_key' => '[developer_key]',
        'username' => [ 'username' ],
        'limit' => 10
    ],
    'twitter' => [
        'username' => '[username]',
        'oauth_access_token' => "[oauth_access_token]",
        'oauth_access_token_secret' => "[oauth_access_token_secret]",
        'consumer_key' => "[consumer_key]",
        'consumer_secret' => "[consumer_secret]",
        'limit' => 5
    ],
    'facebook' => [
        'username' => '[username]',
        'app_id' => '[app_id]',
        'app_secret' => '[app_secret]',
        'default_access_token' => '[default_access_token]',
        'limit' => 10
    ]
]);

$feed->limit = 40;
$feed->includeOriginals = isset($_GET['originals']);
$feed->cacheTime = 60 * 5;
$feed->instagramExpiredCallback = function(\Coho\SocialFeed $feed) {
    $httpAdapter = new Guzzle6HttpAdapter(new Client());
    $sparky = new SparkPost($httpAdapter, ['key' => '[sparkpost_apikey]']);

    $sparky->transmission->send([
        'from' => '[alert_email_from]',
        'text' => 'Sorry, your Instagram access token has expired. Please click the following link to renew it:\n\n' . $feed->getCurrentUrl() . '?action=renew-instagram-token',
        'subject' => 'Your Instagram access token has expired',
        'recipients' => [
            [
                'address' => [
                    'email' => '[token_expired_email]'
                ]
            ]
        ]
    ]);
};

$feed->createPublicApi();
