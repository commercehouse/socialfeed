# Commerce House Social Feed

`\Coho\SocialFeed` is a utility that will pull in posts from twitter, facebook, instagram, and
youtube into a unified JSON feed.

## Usage

Begin by requiring the package using `composer`:

```bash
$ composer require cohodigital/social
```

Then use the `\Coho\SocialFeed` class to construct an object with settings for your social
feeds.

The following example shows how to create up an instance of `SocialFeed` and provide options:

```php
<?php

use \Coho\SocialFeed;

$feed = new SocialFeed([
    'instagram' => [
        'client_id' => '[client_id]',
        'client_secret' => '[client_secret]',
        'limit' => 10
    ],
    'youtube' => [
        'developer_key' => '[developer_key]',
        'username' => [ 'username' ],
        'limit' => 10
    ],
    'twitter' => [
        'username' => '[username]',
        'oauth_access_token' => "[oauth_access_token]",
        'oauth_access_token_secret' => "[oauth_access_token_secret]",
        'consumer_key' => "[consumer_key]",
        'consumer_secret' => "[consumer_secret]",
        'limit' => 10
    ],
    'facebook' => [
        'username' => '[username]',
        'app_id' => '[app_id]',
        'app_secret' => '[app_secret]',
        'default_access_token' => '[default_access_token]',
        'limit' => 10
    ]
]);

$feed->limit = 40;
$feed->includeOriginals = false;
$feed->cacheTime = 60 * 5;
$feed->instagramExpiredCallback = function(SocialFeed $feed) {
    // Alert the user that the Instagram token has expired.
    // You'll probably need to send the user an email.
};
```

With a `SocialFeed` object created, you have two options for usage:

1. **Setup a public JSON ReST endpoint:**

   You can call the `$feed->createPublicApi()` method, which automatically creates a public
   JSON ReST API endpoint. This is easier but is less flexible if you need to do something
   other than what the public API already offers, or have a highly customized setup.

   Use the script example above, and then add the following line:

   ```php
   $feed->createPublicApi();
   ```

   That's it! The current script will become an endpoint that offers all features of
   the `SocialFeed` class. To see usage details, load the script in your browser (without any
   query parameters).

2. **Setup using the php API:**

   You can also use the object oriented API directly, which is more flexible but requires more
   setup.

   Use the script example above, and then use any of the API methods described in detail below.

   A simple example that only provides the JSON feed results would involve using the script
   example above and adding the following lines:

   ```php
   header('Content-Type: application/json');
   print json_encode($feed->getResults());
   ```



## `SocialFeed` class methods

The API offers the following methods:

- #### `new \Coho\SocialFeed($settings)`

  The constructor accepts a single parameter, which is an array of settings for each feed source.
  See the example above for the full set of options.

- #### `$socialFeed->getResults($page = 1, $nocache = false)`

  Returns the social feed results in a php array.

  - `$page` - Indicates the page number to request. Note that you must request pages sequentially
  within the specified cache time limit.

  - `$nocache` - Forces a cache clear before requesting the feeds.

- #### `$socialFeed->renewInstagramToken($setInstagramTokenUrl)`

  Call this method to begin the process of renewing the Instagram access token that is needed
  to read posts from an Instagram user's account.

  **Note:** this method redirects the user to a URL on Instagram's website and stops script
  execution.

  - `$setInstagramTokenUrl` - The url that the user will be redirected back to after successful
  authentication with Instagram. The url will receive a query parameter `code=XXXXX`, which
  is used by the `$socialFeed->setInstagramToken()` method.


- #### `$socialFeed->setInstagramToken($setInstagramTokenUrl, $redirect)`

  This is the second step in the process to renew an instagram token. Call this method in the
  script for the URL that you passed to `renewInstagramToken()`. There must be a query parameter `code=...` present
  in order for this method to work. Instagram passes this parameter automatically to the url specified when you
  call `renewInstagramToken()`.

  - `$setInstagramTokenUrl` - The same url passed to `renewInstagramToken()` (see above).

  - `$redirect` - Indicates where to send the user after successful authentication.


- #### `$socialFeed->clearCache()`

  Clears the cache so that the next call to `getResults()` makes a fresh request to all feed
  sources.


- #### `$socialFeed->createPublicAPI($scriptUrl = null)`

  Creates a public endpoint that enables all the features of `SocialFeed` in a JSON ReST
  service.


# Making Updates & Changes

This utility uses php's *composer* package manager and the *packagist* service (located at http://packagist.org/). Use the login credentials in TeamPassword to access our account.

If you make a change that you wish to deploy, simply use git to tag the commit with a version number "vX.X.X" and push it to bitbucket. Packagist will automatically detect and release an updated version. Note that it make take a few minutes for a `composer update` command to refresh properly.