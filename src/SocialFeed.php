<?php

namespace Coho;

use Underscore\Underscore as _;
use Gregwar\Cache\Cache;

class SocialFeed
{
    /**
     * @var int The total number of results to return.
     */
    public $limit = 20;

    /**
     * @var bool Whether to include original feed data in each result.
     */
    public $includeOriginals = false;

    /**
     * @var int The number of seconds to cache results.
     */
    public $cacheTime = 300;

    /**
     * @var \Callable A method that will be called if the Instagram token is expired.
     */
    public $instagramExpiredCallback = null;

    private $sources = [];

    private $cache = null;

    public function __construct($sources = [])
    {
        $this->sources = _::pick($sources, 'instagram', 'youtube', 'twitter', 'facebook');
        $this->cache = new Cache('cache/coho-social-feed');
    }

    /**
     * @param $setInstagramTokenUrl string The same url passed to `renewInstagramToken()` (@see renewInstagramToken).
     * @param $redirect string Indicates where to send the user after successful authentication.
     */
    public function setInstagramToken($setInstagramTokenUrl, $redirect)
    {
        if (!$_GET['code'])
            die ('code=... must be present as a query parameter in order to call setInstagramToken().');

        $c = curl_init('https://api.instagram.com/oauth/access_token');
        curl_setopt_array($c, [
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => [
                'client_id' => $this->sources['instagram']['client_id'],
                'client_secret' => $this->sources['instagram']['client_secret'],
                'grant_type' => 'authorization_code',
                'redirect_uri' => $setInstagramTokenUrl, //$this->getCurrentUrl(),// $originalRedirect,//$this->getUrlWithParamConnector($scriptUrl) . "action=set-instagram-token&redirect={$tokenParts[1]}",
                'code' => $_GET['code'],
            ]
        ]);

        $json = json_decode(curl_exec($c), true);

        if (!empty($json['access_token']))
        {
            $this->cache->set('instagram_access_token', $json['access_token']);
            header("Location: " . urldecode($redirect));
        }
        else
        {
            echo json_encode($json);
        }
    }

    /**
     * @param $setInstagramTokenUrl string The url that the user will be redirected back to after successful
     * authentication with Instagram. The url will receive a query parameter `code=XXXXX`, which
     * is used by the `$socialFeed->setInstagramToken()` method.
     */
    public function renewInstagramToken($setInstagramTokenUrl)
    {
        header("Location: https://api.instagram.com/oauth/authorize/" .
            "?client_id={$this->sources['instagram']['client_id']}" .
            "&scope=public_content" .
            "&redirect_uri=" . urlencode($setInstagramTokenUrl) .
            "&response_type=code"
        );
        die();
    }

    public function clearCache()
    {
        $dir_posts = dirname(getcwd() . '/' . $this->cache->getCacheFile('posts'));
        $dir_pagination = dirname(getcwd() . '/' . $this->cache->getCacheFile('pagination'));
        foreach (scandir($dir_posts) as $item) {
            if ($item == '.' || $item == '..') continue;
            @unlink("$dir_posts/$item");
        }
        foreach (scandir($dir_pagination) as $item) {
            if ($item == '.' || $item == '..') continue;
            @unlink("$dir_pagination/$item");
        }
    }

    public function getResults($page = 1, $nocache = false)
    {
        $pageData = @json_decode($this->cache->get('pagination_' . $page), true) ?: ['#' => 1];

        $posts = $this->cache->getOrCreate('posts_' . $pageData['#'], [ 'max-age' => $nocache ? 0 : $this->cacheTime ], function() use ($pageData) {

            $results = [];
            $errors = [];

            $nextPageId = $pageData['#'] + 1;
            $nextPageData = [ '#' => $nextPageId ];

            // YOUTUBE

            if (!empty($this->sources['youtube']))
            {
                $client = new \Google_Client();
                $client->setApplicationName('Coho Feed Aggregator');
                $client->setDeveloperKey($this->sources['youtube']['developer_key']);

                $service = new \Google_Service_YouTube($client);

                $playlistItems = [];
                $nextPageData['youtube'] = [];

                foreach ($this->sources['youtube']['username'] as $username)
                {
                    $channels = $service->channels->listChannels('id,contentDetails', [ 'forUsername' => $username ]);

                    $newPlaylistItems = $service->playlistItems->listPlaylistItems('id,contentDetails,snippet', [
                        'pageToken' => !empty($pageData['youtube'][$username]) ? $pageData['youtube'][$username] : '',
                        'maxResults' => $this->sources['youtube']['limit'],
                        'playlistId' => $channels->getItems()[0]['contentDetails']['relatedPlaylists']['uploads']
                    ]);

                    $nextPageData['youtube'][$username] = $newPlaylistItems->getNextPageToken();

                    $newPlaylistItems = $newPlaylistItems->getItems();

                    _::each($newPlaylistItems, function($item) use ($username) {
                        $item->username = $username;
                    });

                    $playlistItems = array_merge($playlistItems, $newPlaylistItems);
                }

                // Sort the playlist items in reverse published date order
                $playlistItems = array_reverse(_::sortBy($playlistItems, function(\Google_Service_YouTube_PlaylistItem $item) {
                    return $item->getContentDetails()['publishedAt'];
                }));

                // Limit to the first N results
                $playlistItems = array_slice($playlistItems, 0, $this->sources['youtube']['limit']);

                $videos = $service->videos->listVideos('id,statistics', [
                    'id' => implode(',', _::map($playlistItems, function(\Google_Service_YouTube_PlaylistItem $item) {
                        return $item->getContentDetails()['videoId'];
                    }))
                ])->getItems();

                $videos = _::indexBy($videos, function(\Google_Service_YouTube_Video $item) { return $item->getId(); });

                $results[] = _::map($playlistItems, function(\Google_Service_YouTube_PlaylistItem $item) use ($videos) {
                    /** @var \Google_Service_YouTube_Video $video */
                    $video = $videos[$item->getContentDetails()['videoId']];
                    return [
                        'title' => $item->getSnippet()['title'],
                        'description' => $item->getSnippet()['description'],
                        'date' => (new \DateTime($item->getSnippet()['publishedAt']))->format(DATE_ISO8601),
                        'image' => [
                            'url' => $item->getSnippet()['thumbnails']['high']->getUrl(),
                            'width' => $item->getSnippet()['thumbnails']['high']->getWidth(),
                            'height' => $item->getSnippet()['thumbnails']['high']->getHeight()
                        ],
                        'type' => 'youtube',
                        'id' => $item->getSnippet()['resourceId']['videoId'],
                        'user' => $item->username,
                        'link' => 'https://youtube.com/watch?v=' . $item->getSnippet()['resourceId']['videoId'],
                        'likes' => (int)$video->getStatistics()['favoriteCount'],
                        'comments' => (int)$video->getStatistics()['commentCount'],
                        '_original' => $this->includeOriginals ? [
                            'snippet' => $item->getSnippet(),
                            'contentDetails' => $item->getContentDetails(),
                            'statistics' => $video->getStatistics()
                        ] : null
                    ];
                });
            }



            // TWITTER

            if (!empty($this->sources['twitter']))
            {
                $twitterapi = new \TwitterAPIExchange([
                    'oauth_access_token' => $this->sources['twitter']['oauth_access_token'],
                    'oauth_access_token_secret' => $this->sources['twitter']['oauth_access_token_secret'],
                    'consumer_key' => $this->sources['twitter']['consumer_key'],
                    'consumer_secret' => $this->sources['twitter']['consumer_secret']
                ]);

                $twitter = json_decode($twitterapi->setGetfield("screen_name={$this->sources['twitter']['username']}&count={$this->sources['twitter']['limit']}" . (!empty($pageData['twitter']) ? "&max_id={$pageData['twitter']}" : ''))
                    ->buildOauth('https://api.twitter.com/1.1/statuses/user_timeline.json', 'GET')
                    ->performRequest(), true);

                $nextPageData['twitter'] = _::min(_::map($twitter, function($t) { return $t['id']; }));

                $results[] = _::map($twitter, function ($item) {
                    return [
                        'title' => html_entity_decode($item['text']),
                        'description' => _::call(function () use ($item)
                        {
                            $text = html_entity_decode($item['text']);
                            $entities = _::flatten(_::map($item['entities'], function ($value, $key)
                            {
                                return _::map($value, function ($v) use ($key)
                                {
                                    $v['type'] = $key;
                                    return $v;
                                });
                            }), 1);
                            $entities = array_reverse(_::sortBy($entities, function ($entity) { return $entity['indices'][0]; }));
                            return trim(_::reduce($entities, function ($text, $entity) {
                                switch ($entity['type'])
                                {
                                    case 'user_mentions':
                                        return $this->mb_substr_replace($text, '<a href="https://twitter.com/' . $entity['screen_name'] . '" target="_blank">@' . $entity['screen_name'] . '</a>', $entity['indices'][0], $entity['indices'][1] - $entity['indices'][0]);

                                    case 'urls':
                                        return $this->mb_substr_replace($text, '<a href="' . $entity['url'] . '" target="_blank">' . $entity['display_url'] . '</a>', $entity['indices'][0], $entity['indices'][1] - $entity['indices'][0]);

                                    case 'hashtags':
                                        return $this->mb_substr_replace($text, '<a href="https://twitter.com/hashtag/' . $entity['text'] . '" target="_blank">#' . $entity['text'] . '</a>', $entity['indices'][0], $entity['indices'][1] - $entity['indices'][0]);

                                    case 'media':
                                        return $this->mb_substr_replace($text, '', $entity['indices'][0], $entity['indices'][1] - $entity['indices'][0]);

                                    case 'symbols':
                                        // ?
                                        break;
                                }
                                return $text;
                            }, $text));
                        }),
                        'date' => (new \DateTime($item['created_at']))->format(DATE_ISO8601),
                        'image' => isset($item['entities']['media']) && count($item['entities']['media']) ? [
                            'url' => $item['entities']['media'][0]['media_url_https'] . ':large',
                            'width' => $item['entities']['media'][0]['sizes']['large']['w'],
                            'height' => $item['entities']['media'][0]['sizes']['large']['h']
                        ] : null,
                        'type' => 'twitter',
                        'id' => $item['id'],
                        'user' => $item['user']['screen_name'],
                        'likes' => $item['favorite_count'],
                        'comments' => $item['retweet_count'],
                        'link' => "https://twitter.com/{$this->sources['twitter']['username']}/status/{$item['id']}",
                        '_original' => $this->includeOriginals ? $item : null
                    ];
                });
            }


            // FACEBOOK

            if (!empty($this->sources['facebook']))
            {
                $fb = new \Facebook\Facebook([
                    'app_id' => $this->sources['facebook']['app_id'],
                    'app_secret' => $this->sources['facebook']['app_secret'],
                    'default_graph_version' => 'v2.5',
                    'default_access_token' => $this->sources['facebook']['default_access_token']
                ]);

                $request = $fb->request('GET', !empty($pageData['facebook']) ? str_replace('https://graph.facebook.com/v2.5', '', $pageData['facebook']) : "/{$this->sources['facebook']['username']}/posts?limit={$this->sources['facebook']['limit']}&fields=" . implode(',', [
                        'id',
                        'attachments',
                        'likes.limit(1).summary(true)',
                        'comments.limit(1).summary(true)',
                        'created_time',
                        'link',
                        'message',
                        'type'
                    ]));
                $response = $fb->getClient()->sendRequest($request);

                $facebook = $response->getGraphEdge();

                $nextPageData['facebook'] = $facebook->getMetaData()['paging']['next'];

                $results[] = _::map($facebook, function(\Facebook\GraphNodes\GraphNode $item) {
                    $graph = $item;
                    $item = $item->asArray();
                    return [
                        'title' => !empty($item['message']) ? (strstr($item['message'], "\n", true) ?: $item['message']) : '',
                        'description' => !empty($item['message']) ? $item['message'] : '',
                        'date' => $item['created_time']->format(DATE_ISO8601),
                        'image' => _::call(function() use ($item) {
                            if (empty($item['attachments'][0]['media']))
                                return null;
                            $media = _::first($item['attachments'][0]['media']);
                            return [
                                'url' => $media['src'],
                                'width' => $media['width'],
                                'height' => $media['height'],
                            ];
                        }),
                        'type' => 'facebook',
                        'id' => $item['id'],
                        'user' => $this->sources['facebook']['username'],
                        'link' => !empty($item['link']) ? $item['link'] : '',
                        'likes' => $graph->getField('likes')->getMetaData()['summary']['total_count'],
                        'comments' => $graph->getField('comments')->getMetaData()['summary']['total_count'],
                        '_original' => $this->includeOriginals ? $item : null
                    ];
                });
            }


            // INSTAGRAM

            if (!empty($this->sources['instagram']))
            {
                if ($instagram_access_token = $this->cache->get('instagram_access_token'))
                {
                    $c = curl_init("https://api.instagram.com/v1/users/self?access_token=$instagram_access_token");
                    curl_setopt_array($c, [ CURLOPT_RETURNTRANSFER => true ]);
                    $json = json_decode(curl_exec($c), true);

                    if (!empty($json['meta']['code']) && $json['meta']['code'] == 400)
                    {
                        if (!$this->cache->get('instagram_expired_notification'))
                        {
                            $this->cache->set('instagram_expired_notification', 'true');
                            if ($cb = $this->instagramExpiredCallback)
                                $cb($this);
                        }

                        $errors['instagram'] = $json;
                    }

                    else if (empty($json['data']['id']))
                    {
                        $errors['instagram'] = $json;
                    }

                    else
                    {
                        $this->cache->set('instagram_expired_notification');

                        $c = curl_init("https://api.instagram.com/v1/users/{$json['data']['id']}/media/recent?count={$this->sources['instagram']['limit']}&access_token=$instagram_access_token&max_id=" . (!empty($pageData['instagram']) ? $pageData['instagram'] : ''));
                        curl_setopt_array($c, [ CURLOPT_RETURNTRANSFER => true ]);
                        $instagram = json_decode(curl_exec($c), true);

                        $nextPageData['instagram'] = _::min(_::map($instagram['data'], function($i) { return $i['id']; }));

                        $results[] = _::map($instagram['data'], function($item) {
                            return [
                                'title' => $item['caption']['text'] ?: '',
                                'description' => preg_replace_callback('/(#|@)(.+?)( |$)/', function($matches) {
                                    if ($matches[1] == '#')
                                        return '<a href="https://www.instagram.com/explore/tags/' . $matches[2] . '/">' . $matches[1] . $matches[2] . '</a>' . $matches[3];
                                    if ($matches[1] == '@')
                                        return '<a href="https://www.instagram.com/' . $matches[2] . '/">' . $matches[1] . $matches[2] . '</a>' . $matches[3];
                                }, $item['caption']['text'] ?: ''),
                                'date' => \DateTime::createFromFormat('U', $item['created_time'])->format(DATE_ISO8601),
                                'image' => $item['images']['standard_resolution'],
                                'type' => 'instagram',
                                'id' => $item['id'],
                                'user' => $item['user']['username'],
                                'link' => $item['link'],
                                'likes' => $item['likes']['count'],
                                'comments' => $item['comments']['count'],
                                '_original' => $this->includeOriginals ? $item : null
                            ];
                        });
                    }
                }
            }

            $this->cache->set('pagination_' . $nextPageId, json_encode($nextPageData));

            return json_encode([
                'data' => array_slice(
                    array_reverse(
                        _::sortBy(
                            _::values(_::flatten($results, true)),
                            function($item) { return $item['date']; }
                        )
                    ),
                    0,
                    $this->limit
                ),
                'errors' => $errors,
                'nextPage' => $nextPageId
            ]);
        });

        return [ 'posts' => json_decode($posts, true) ];
    }


    /**
     * Enables a public API endpoint for the social feed.
     *
     * @param string $scriptUrl The full url to the endpoint for the API. Uses the current request url if null.
     * @param string $instagramRenewalRedirect The url to send the user to after a successful Instagram token renewal authentication.
     */
    public function createPublicApi($scriptUrl = null, $instagramRenewalRedirect = null)
    {
        $scriptUrl = $scriptUrl ?: $this->getCurrentUrl();

        switch (!empty($_GET['action']) ? $_GET['action'] : '')
        {
            case 'set-instagram-token':
                $this->setInstagramToken($this->getUrlWithParamConnector($scriptUrl) . 'action=set-instagram-token', $instagramRenewalRedirect);
                break;

            case 'renew-instagram-token':
                $this->renewInstagramToken($this->getUrlWithParamConnector($scriptUrl) . 'action=set-instagram-token');
                break;


            case 'clear-cache':
                $this->clearCache();
                break;


            case 'feed':
                $results = $this->getResults(!empty($_GET['page']) ? $_GET['page'] : 1, !empty($_GET['nocache']));

                if ($results && !error_get_last())
                {
                    header('Content-Type: application/json; charset=utf-8');
                    echo json_encode($results);
                }

                break;



            default:

                ?>

                <h1>Usage:</h1>

                <dl>
                    <dt><a href="?action=feed&page=1">/social.php?action=feed&page=X</a></dt>
                    <dd>
                        <p>View the JSON feed.</p>
                        <p>The "page" parameter specifies which results page to return. Note that pages must be requested sequentially to
                            function properly.</p>
                    </dd>

                    <dt><a href="?action=clear-cache">/social.php?action=clear-cache</a></dt>
                    <dd>
                        <p>Clear the cache.</p>
                    </dd>

                    <dt><a href="?action=renew-instagram-token">/social.php?action=renew-instagram-token</a></dt>
                    <dd>
                        <p>Renews the Instagram access token. This will redirect to an instagram login or approval page.</p>
                        <p>You must login and approve the Instagram application using the user you with to pull posts from.</p>
                    </dd>

                    <dt><a href="?">/social.php</a></dt>
                    <dd>
                        <p>View this help page.</p>
                    </dd>
                </dl>

                <?php

                break;
        }
    }


    // UTILITY

    public function getCurrentUrl()
    {
        return 'http' . (!empty($_SERVER['HTTPS']) ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . (strpos($_SERVER['REQUEST_URI'], '?') === false ? $_SERVER['REQUEST_URI'] : substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')));
    }

    private function getUrlWithParamConnector($url)
    {
        return $url . (strpos($url, '?') !== false ? '&' : '?');
    }

    private function mb_substr_replace($string, $replacement, $start, $length = 0)
    {
        $result  = mb_substr($string, 0, $start, 'UTF-8');
        $result .= $replacement;

        if ($length > 0) {
            $result .= mb_substr($string, ($start+$length), mb_strlen($string, 'UTF-8'), 'UTF-8');
        }

        return $result;
    }
}
